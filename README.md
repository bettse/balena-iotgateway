# Balena IoT Gateway

- `TZ` America/Los_Angeles

## Threshold

Runs IoT(Ble) gateway

https://gitlab.com/bettse/threshold

### ENVs

- `NRF_CONNECT_USERNAME`
- `NRF_CONNECT_PASSWORD`
